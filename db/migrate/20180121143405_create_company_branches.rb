class CreateCompanyBranches < ActiveRecord::Migration[5.1]
  def change
    create_table :company_branches do |t|
      t.string :name
      t.string :street1
      t.string :street2
      t.string :postal_code
      t.string :city
      t.string :state
      t.string :country
      t.references :company_headquarter, foreign_key: true

      t.timestamps
    end
  end
end
