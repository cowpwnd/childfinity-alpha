class CreateEmployeeStaffAddresses < ActiveRecord::Migration[5.1]
  def change
    create_table :employee_staff_addresses do |t|
      t.references :employee_staff_detail, foreign_key: true
      t.string :street1
      t.string :street2
      t.string :postal
      t.string :city
      t.string :state
      t.string :country

      t.timestamps
    end
  end
end
