class CreateCompanyAdvertisements < ActiveRecord::Migration[5.1]
  def change
    create_table :company_advertisements do |t|
      t.string :name
      t.string :description
      t.binary :ads_image
      t.integer :ads_duration
      t.boolean :ads_active
      t.references :company_branch, foreign_key: true

      t.timestamps
    end
  end
end
