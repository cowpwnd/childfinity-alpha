class CreateEmployeeStaffDetails < ActiveRecord::Migration[5.1]
  def change
    create_table :employee_staff_details do |t|
      t.string :first_name
      t.string :last_name
      t.string :gender
      t.string :nric
      t.date :dob
      t.string :socso_id
      t.string :phone_number
      t.string :spouse_name
      t.string :spouse_phone_number

      t.timestamps
    end
  end
end
