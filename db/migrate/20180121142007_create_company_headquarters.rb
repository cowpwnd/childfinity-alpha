class CreateCompanyHeadquarters < ActiveRecord::Migration[5.1]
  def change
    create_table :company_headquarters do |t|
      t.string :name
      t.string :registration_id

      t.timestamps
    end
  end
end
