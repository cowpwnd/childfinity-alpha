# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180121144128) do

  create_table "company_advertisements", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.string "description"
    t.binary "ads_image"
    t.integer "ads_duration"
    t.boolean "ads_active"
    t.bigint "company_branch_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["company_branch_id"], name: "index_company_advertisements_on_company_branch_id"
  end

  create_table "company_branches", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.string "street1"
    t.string "street2"
    t.string "postal_code"
    t.string "city"
    t.string "state"
    t.string "country"
    t.bigint "company_headquarter_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["company_headquarter_id"], name: "index_company_branches_on_company_headquarter_id"
  end

  create_table "company_headquarters", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.string "registration_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "employee_roles", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "employee_staff_accounts", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.bigint "employee_staff_detail_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_employee_staff_accounts_on_email", unique: true
    t.index ["employee_staff_detail_id"], name: "index_employee_staff_accounts_on_employee_staff_detail_id"
    t.index ["reset_password_token"], name: "index_employee_staff_accounts_on_reset_password_token", unique: true
  end

  create_table "employee_staff_addresses", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "employee_staff_detail_id"
    t.string "street1"
    t.string "street2"
    t.string "postal"
    t.string "city"
    t.string "state"
    t.string "country"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["employee_staff_detail_id"], name: "index_employee_staff_addresses_on_employee_staff_detail_id"
  end

  create_table "employee_staff_details", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "first_name"
    t.string "last_name"
    t.string "gender"
    t.string "nric"
    t.date "dob"
    t.string "socso_id"
    t.string "phone_number"
    t.string "spouse_name"
    t.string "spouse_phone_number"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "company_advertisements", "company_branches"
  add_foreign_key "company_branches", "company_headquarters"
  add_foreign_key "employee_staff_addresses", "employee_staff_details"
end
