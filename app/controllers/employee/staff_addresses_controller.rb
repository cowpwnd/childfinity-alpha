class Employee::StaffAddressesController < ApplicationController
  before_action :set_employee_staff_address, only: [:show, :edit, :update, :destroy]

  # GET /employee/staff_addresses
  # GET /employee/staff_addresses.json
  def index
    @employee_staff_addresses = Employee::StaffAddress.all
  end

  # GET /employee/staff_addresses/1
  # GET /employee/staff_addresses/1.json
  def show
  end

  # GET /employee/staff_addresses/new
  def new
    @employee_staff_address = Employee::StaffAddress.new
  end

  # GET /employee/staff_addresses/1/edit
  def edit
  end

  # POST /employee/staff_addresses
  # POST /employee/staff_addresses.json
  def create
    @employee_staff_address = Employee::StaffAddress.new(employee_staff_address_params)

    respond_to do |format|
      if @employee_staff_address.save
        format.html { redirect_to @employee_staff_address, notice: 'Staff address was successfully created.' }
        format.json { render :show, status: :created, location: @employee_staff_address }
      else
        format.html { render :new }
        format.json { render json: @employee_staff_address.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /employee/staff_addresses/1
  # PATCH/PUT /employee/staff_addresses/1.json
  def update
    respond_to do |format|
      if @employee_staff_address.update(employee_staff_address_params)
        format.html { redirect_to @employee_staff_address, notice: 'Staff address was successfully updated.' }
        format.json { render :show, status: :ok, location: @employee_staff_address }
      else
        format.html { render :edit }
        format.json { render json: @employee_staff_address.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /employee/staff_addresses/1
  # DELETE /employee/staff_addresses/1.json
  def destroy
    @employee_staff_address.destroy
    respond_to do |format|
      format.html { redirect_to employee_staff_addresses_url, notice: 'Staff address was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_employee_staff_address
      @employee_staff_address = Employee::StaffAddress.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def employee_staff_address_params
      params.require(:employee_staff_address).permit(:employee_staff_detail_id, :street1, :street2, :postal, :city, :state, :country)
    end
end
