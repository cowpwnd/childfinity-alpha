class Employee::StaffDetailsController < ApplicationController
  before_action :set_employee_staff_detail, only: [:show, :edit, :update, :destroy]

  # GET /employee/staff_details
  # GET /employee/staff_details.json
  def index
    @employee_staff_details = Employee::StaffDetail.all
  end

  # GET /employee/staff_details/1
  # GET /employee/staff_details/1.json
  def show
  end

  # GET /employee/staff_details/new
  def new
    @employee_staff_detail = Employee::StaffDetail.new
  end

  # GET /employee/staff_details/1/edit
  def edit
  end

  # POST /employee/staff_details
  # POST /employee/staff_details.json
  def create
    @employee_staff_detail = Employee::StaffDetail.new(employee_staff_detail_params)

    respond_to do |format|
      if @employee_staff_detail.save
        format.html { redirect_to @employee_staff_detail, notice: 'Staff detail was successfully created.' }
        format.json { render :show, status: :created, location: @employee_staff_detail }
      else
        format.html { render :new }
        format.json { render json: @employee_staff_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /employee/staff_details/1
  # PATCH/PUT /employee/staff_details/1.json
  def update
    respond_to do |format|
      if @employee_staff_detail.update(employee_staff_detail_params)
        format.html { redirect_to @employee_staff_detail, notice: 'Staff detail was successfully updated.' }
        format.json { render :show, status: :ok, location: @employee_staff_detail }
      else
        format.html { render :edit }
        format.json { render json: @employee_staff_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /employee/staff_details/1
  # DELETE /employee/staff_details/1.json
  def destroy
    @employee_staff_detail.destroy
    respond_to do |format|
      format.html { redirect_to employee_staff_details_url, notice: 'Staff detail was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_employee_staff_detail
      @employee_staff_detail = Employee::StaffDetail.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def employee_staff_detail_params
      params.require(:employee_staff_detail).permit(:first_name, :last_name, :gender, :nric, :dob, :socso_id, :phone_number, :spouse_name, :spouse_phone_number)
    end
end
