class Company::BranchesController < ApplicationController
  before_action :set_company_branch, only: [:show, :edit, :update, :destroy]

  # GET /company/branches
  # GET /company/branches.json
  def index
    @company_branches = Company::Branch.all
  end

  # GET /company/branches/1
  # GET /company/branches/1.json
  def show
  end

  # GET /company/branches/new
  def new
    @company_branch = Company::Branch.new
  end

  # GET /company/branches/1/edit
  def edit
  end

  # POST /company/branches
  # POST /company/branches.json
  def create
    @company_branch = Company::Branch.new(company_branch_params)

    respond_to do |format|
      if @company_branch.save
        format.html { redirect_to @company_branch, notice: 'Branch was successfully created.' }
        format.json { render :show, status: :created, location: @company_branch }
      else
        format.html { render :new }
        format.json { render json: @company_branch.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /company/branches/1
  # PATCH/PUT /company/branches/1.json
  def update
    respond_to do |format|
      if @company_branch.update(company_branch_params)
        format.html { redirect_to @company_branch, notice: 'Branch was successfully updated.' }
        format.json { render :show, status: :ok, location: @company_branch }
      else
        format.html { render :edit }
        format.json { render json: @company_branch.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /company/branches/1
  # DELETE /company/branches/1.json
  def destroy
    @company_branch.destroy
    respond_to do |format|
      format.html { redirect_to company_branches_url, notice: 'Branch was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_company_branch
      @company_branch = Company::Branch.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def company_branch_params
      params.require(:company_branch).permit(:name, :street1, :street2, :postal_code, :city, :state, :country, :company_headquarter_id)
    end
end
