class Company::HeadquartersController < ApplicationController
  before_action :set_company_headquarter, only: [:show, :edit, :update, :destroy]

  # GET /company/headquarters
  # GET /company/headquarters.json
  def index
    @company_headquarters = Company::Headquarter.all
  end

  # GET /company/headquarters/1
  # GET /company/headquarters/1.json
  def show
  end

  # GET /company/headquarters/new
  def new
    @company_headquarter = Company::Headquarter.new
  end

  # GET /company/headquarters/1/edit
  def edit
  end

  # POST /company/headquarters
  # POST /company/headquarters.json
  def create
    @company_headquarter = Company::Headquarter.new(company_headquarter_params)

    respond_to do |format|
      if @company_headquarter.save
        format.html { redirect_to @company_headquarter, notice: 'Headquarter was successfully created.' }
        format.json { render :show, status: :created, location: @company_headquarter }
      else
        format.html { render :new }
        format.json { render json: @company_headquarter.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /company/headquarters/1
  # PATCH/PUT /company/headquarters/1.json
  def update
    respond_to do |format|
      if @company_headquarter.update(company_headquarter_params)
        format.html { redirect_to @company_headquarter, notice: 'Headquarter was successfully updated.' }
        format.json { render :show, status: :ok, location: @company_headquarter }
      else
        format.html { render :edit }
        format.json { render json: @company_headquarter.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /company/headquarters/1
  # DELETE /company/headquarters/1.json
  def destroy
    @company_headquarter.destroy
    respond_to do |format|
      format.html { redirect_to company_headquarters_url, notice: 'Headquarter was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_company_headquarter
      @company_headquarter = Company::Headquarter.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def company_headquarter_params
      params.require(:company_headquarter).permit(:name, :registration_id)
    end
end
