class Company::AdvertisementsController < ApplicationController
  before_action :set_company_advertisement, only: [:show, :edit, :update, :destroy]

  # GET /company/advertisements
  # GET /company/advertisements.json
  def index
    @company_advertisements = Company::Advertisement.all
  end

  # GET /company/advertisements/1
  # GET /company/advertisements/1.json
  def show
  end

  # GET /company/advertisements/new
  def new
    @company_advertisement = Company::Advertisement.new
  end

  # GET /company/advertisements/1/edit
  def edit
  end

  # POST /company/advertisements
  # POST /company/advertisements.json
  def create
    @company_advertisement = Company::Advertisement.new(company_advertisement_params)

    respond_to do |format|
      if @company_advertisement.save
        format.html { redirect_to @company_advertisement, notice: 'Advertisement was successfully created.' }
        format.json { render :show, status: :created, location: @company_advertisement }
      else
        format.html { render :new }
        format.json { render json: @company_advertisement.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /company/advertisements/1
  # PATCH/PUT /company/advertisements/1.json
  def update
    respond_to do |format|
      if @company_advertisement.update(company_advertisement_params)
        format.html { redirect_to @company_advertisement, notice: 'Advertisement was successfully updated.' }
        format.json { render :show, status: :ok, location: @company_advertisement }
      else
        format.html { render :edit }
        format.json { render json: @company_advertisement.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /company/advertisements/1
  # DELETE /company/advertisements/1.json
  def destroy
    @company_advertisement.destroy
    respond_to do |format|
      format.html { redirect_to company_advertisements_url, notice: 'Advertisement was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_company_advertisement
      @company_advertisement = Company::Advertisement.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def company_advertisement_params
      params.require(:company_advertisement).permit(:name, :description, :ads_image, :ads_duration, :ads_active, :company_branch_id)
    end
end
