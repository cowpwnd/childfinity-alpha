class PagesController < ApplicationController
  def homepage
  end

  def index
  end

  def about_us
  end

  def contact
  end

  def enquiry
  end
end
