json.extract! employee_staff_address, :id, :employee_staff_detail_id, :street1, :street2, :postal, :city, :state, :country, :created_at, :updated_at
json.url employee_staff_address_url(employee_staff_address, format: :json)
