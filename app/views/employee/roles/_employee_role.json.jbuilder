json.extract! employee_role, :id, :name, :created_at, :updated_at
json.url employee_role_url(employee_role, format: :json)
