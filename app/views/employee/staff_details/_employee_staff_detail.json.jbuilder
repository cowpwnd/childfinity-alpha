json.extract! employee_staff_detail, :id, :first_name, :last_name, :gender, :nric, :dob, :socso_id, :phone_number, :spouse_name, :spouse_phone_number, :created_at, :updated_at
json.url employee_staff_detail_url(employee_staff_detail, format: :json)
