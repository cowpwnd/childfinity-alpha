json.extract! company_branch, :id, :name, :street1, :street2, :postal_code, :city, :state, :country, :company_headquarter_id, :created_at, :updated_at
json.url company_branch_url(company_branch, format: :json)
