json.extract! company_advertisement, :id, :name, :description, :ads_image, :ads_duration, :ads_active, :company_branch_id, :created_at, :updated_at
json.url company_advertisement_url(company_advertisement, format: :json)
