json.extract! company_headquarter, :id, :name, :registration_id, :created_at, :updated_at
json.url company_headquarter_url(company_headquarter, format: :json)
