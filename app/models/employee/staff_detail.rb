class Employee::StaffDetail < ApplicationRecord
  has_many :employee_staff_addresses, :class_name => 'Employee::StaffAddress', foreign_key: 'employee_staff_detail_id'
  has_one :employee_staff_account, :class_name => 'Employee::StaffAccount', foreign_key: 'employee_staff_detail_id'
end
