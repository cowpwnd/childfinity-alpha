class Company::Branch < ApplicationRecord
  belongs_to :company_headquarter, :class_name => 'Company::Headquarter'
  has_many :company_advertisements, :class_name => 'Company::Advertisement', foreign_key: 'company_branch_id'
end
