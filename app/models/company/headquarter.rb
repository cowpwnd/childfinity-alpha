class Company::Headquarter < ApplicationRecord
  has_many :company_branches, :class_name => 'Company::Branch', foreign_key: 'company_headquarter_id'

end
