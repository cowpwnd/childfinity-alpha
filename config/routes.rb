Rails.application.routes.draw do
  get 'pages/homepage'

  get 'pages/about_us'

  get 'pages/index'

  get 'pages/about-us'

  get 'pages/contact'

  get 'pages/enquiry'

  root to: 'pages#homepage'

  devise_for :staff_accounts, class_name: "Employee::StaffAccount"

  namespace :employee do
    resources :staff_addresses
  end
  namespace :employee do
    resources :staff_details
  end
  namespace :employee do
    resources :roles
  end
  namespace :company do
    resources :advertisements
  end
  namespace :company do
    resources :branches
  end
  namespace :company do
    resources :headquarters
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
