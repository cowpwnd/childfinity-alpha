require "rails_helper"

RSpec.describe Employee::StaffDetailsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/employee/staff_details").to route_to("employee/staff_details#index")
    end

    it "routes to #new" do
      expect(:get => "/employee/staff_details/new").to route_to("employee/staff_details#new")
    end

    it "routes to #show" do
      expect(:get => "/employee/staff_details/1").to route_to("employee/staff_details#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/employee/staff_details/1/edit").to route_to("employee/staff_details#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/employee/staff_details").to route_to("employee/staff_details#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/employee/staff_details/1").to route_to("employee/staff_details#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/employee/staff_details/1").to route_to("employee/staff_details#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/employee/staff_details/1").to route_to("employee/staff_details#destroy", :id => "1")
    end

  end
end
