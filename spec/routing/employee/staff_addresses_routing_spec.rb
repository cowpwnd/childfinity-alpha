require "rails_helper"

RSpec.describe Employee::StaffAddressesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/employee/staff_addresses").to route_to("employee/staff_addresses#index")
    end

    it "routes to #new" do
      expect(:get => "/employee/staff_addresses/new").to route_to("employee/staff_addresses#new")
    end

    it "routes to #show" do
      expect(:get => "/employee/staff_addresses/1").to route_to("employee/staff_addresses#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/employee/staff_addresses/1/edit").to route_to("employee/staff_addresses#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/employee/staff_addresses").to route_to("employee/staff_addresses#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/employee/staff_addresses/1").to route_to("employee/staff_addresses#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/employee/staff_addresses/1").to route_to("employee/staff_addresses#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/employee/staff_addresses/1").to route_to("employee/staff_addresses#destroy", :id => "1")
    end

  end
end
