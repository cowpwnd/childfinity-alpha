require "rails_helper"

RSpec.describe Employee::RolesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/employee/roles").to route_to("employee/roles#index")
    end

    it "routes to #new" do
      expect(:get => "/employee/roles/new").to route_to("employee/roles#new")
    end

    it "routes to #show" do
      expect(:get => "/employee/roles/1").to route_to("employee/roles#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/employee/roles/1/edit").to route_to("employee/roles#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/employee/roles").to route_to("employee/roles#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/employee/roles/1").to route_to("employee/roles#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/employee/roles/1").to route_to("employee/roles#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/employee/roles/1").to route_to("employee/roles#destroy", :id => "1")
    end

  end
end
