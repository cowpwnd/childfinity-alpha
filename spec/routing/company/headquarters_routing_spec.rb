require "rails_helper"

RSpec.describe Company::HeadquartersController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/company/headquarters").to route_to("company/headquarters#index")
    end

    it "routes to #new" do
      expect(:get => "/company/headquarters/new").to route_to("company/headquarters#new")
    end

    it "routes to #show" do
      expect(:get => "/company/headquarters/1").to route_to("company/headquarters#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/company/headquarters/1/edit").to route_to("company/headquarters#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/company/headquarters").to route_to("company/headquarters#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/company/headquarters/1").to route_to("company/headquarters#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/company/headquarters/1").to route_to("company/headquarters#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/company/headquarters/1").to route_to("company/headquarters#destroy", :id => "1")
    end

  end
end
