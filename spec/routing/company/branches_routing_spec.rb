require "rails_helper"

RSpec.describe Company::BranchesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/company/branches").to route_to("company/branches#index")
    end

    it "routes to #new" do
      expect(:get => "/company/branches/new").to route_to("company/branches#new")
    end

    it "routes to #show" do
      expect(:get => "/company/branches/1").to route_to("company/branches#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/company/branches/1/edit").to route_to("company/branches#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/company/branches").to route_to("company/branches#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/company/branches/1").to route_to("company/branches#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/company/branches/1").to route_to("company/branches#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/company/branches/1").to route_to("company/branches#destroy", :id => "1")
    end

  end
end
