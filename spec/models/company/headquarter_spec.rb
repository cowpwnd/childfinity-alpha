require 'rails_helper'

RSpec.describe Company::Headquarter, type: :model do
  it { should have_many(:company_branches).class_name('Company::Branch').with_foreign_key('company_headquarter_id')}
end
