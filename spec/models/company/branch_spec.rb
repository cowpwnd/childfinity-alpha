require 'rails_helper'

RSpec.describe Company::Branch, type: :model do
  it { should belong_to(:company_headquarter)}
  it { should have_many(:company_advertisements).class_name('Company::Advertisement').with_foreign_key('company_branch_id')}
end
