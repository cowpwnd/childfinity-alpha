require 'rails_helper'

RSpec.describe Employee::StaffDetail, type: :model do
  it { should have_many(:employee_staff_addresses).class_name('Employee::StaffAddress').with_foreign_key('employee_staff_detail_id')}
  it { should have_one(:employee_staff_account).class_name('Employee::StaffAccount').with_foreign_key('employee_staff_detail_id')}
end
