require 'rails_helper'

RSpec.describe "employee/staff_addresses/show", type: :view do
  before(:each) do
    @employee_staff_detail = assign(:employee_staff_detail, Employee::StaffDetail.create!(
        :first_name => "Person"
    ))
    @employee_staff_address = assign(:employee_staff_address, Employee::StaffAddress.create!(
      :employee_staff_detail => @employee_staff_detail,
      :street1 => "Street1",
      :street2 => "Street2",
      :postal => "Postal",
      :city => "City",
      :state => "State",
      :country => "Country"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(/Street1/)
    expect(rendered).to match(/Street2/)
    expect(rendered).to match(/Postal/)
    expect(rendered).to match(/City/)
    expect(rendered).to match(/State/)
    expect(rendered).to match(/Country/)
  end
end
