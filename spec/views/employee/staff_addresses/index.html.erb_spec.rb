require 'rails_helper'

RSpec.describe "employee/staff_addresses/index", type: :view do
  before(:each) do
    @employee_staff_detail = assign(:employee_staff_detail, Employee::StaffDetail.create!(
        :first_name => "Person"
    ))
    assign(:employee_staff_addresses, [
      Employee::StaffAddress.create!(
        :employee_staff_detail => @employee_staff_detail,
        :street1 => "Street1",
        :street2 => "Street2",
        :postal => "Postal",
        :city => "City",
        :state => "State",
        :country => "Country"
      ),
      Employee::StaffAddress.create!(
        :employee_staff_detail => @employee_staff_detail,
        :street1 => "Street1",
        :street2 => "Street2",
        :postal => "Postal",
        :city => "City",
        :state => "State",
        :country => "Country"
      )
    ])
  end

  it "renders a list of employee/staff_addresses" do
    render
    assert_select "tr>td", :text => "Person".to_s, :count => 2
    assert_select "tr>td", :text => "Street1".to_s, :count => 2
    assert_select "tr>td", :text => "Street2".to_s, :count => 2
    assert_select "tr>td", :text => "Postal".to_s, :count => 2
    assert_select "tr>td", :text => "City".to_s, :count => 2
    assert_select "tr>td", :text => "State".to_s, :count => 2
    assert_select "tr>td", :text => "Country".to_s, :count => 2
  end
end
