require 'rails_helper'

RSpec.describe "employee/staff_addresses/new", type: :view do
  before(:each) do
    assign(:employee_staff_address, Employee::StaffAddress.new(
      :employee_staff_detail => nil,
      :street1 => "MyString",
      :street2 => "MyString",
      :postal => "MyString",
      :city => "MyString",
      :state => "MyString",
      :country => "MyString"
    ))
  end

  it "renders new employee_staff_address form" do
    render

    assert_select "form[action=?][method=?]", employee_staff_addresses_path, "post" do

      assert_select "input[name=?]", "employee_staff_address[employee_staff_detail_id]"

      assert_select "input[name=?]", "employee_staff_address[street1]"

      assert_select "input[name=?]", "employee_staff_address[street2]"

      assert_select "input[name=?]", "employee_staff_address[postal]"

      assert_select "input[name=?]", "employee_staff_address[city]"

      assert_select "input[name=?]", "employee_staff_address[state]"

      assert_select "input[name=?]", "employee_staff_address[country]"
    end
  end
end
