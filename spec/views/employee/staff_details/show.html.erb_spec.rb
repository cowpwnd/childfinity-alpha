require 'rails_helper'

RSpec.describe "employee/staff_details/show", type: :view do
  before(:each) do
    @employee_staff_detail = assign(:employee_staff_detail, Employee::StaffDetail.create!(
      :first_name => "First Name",
      :last_name => "Last Name",
      :gender => "Gender",
      :nric => "Nric",
      :socso_id => "Socso",
      :phone_number => "Phone Number",
      :spouse_name => "Spouse Name",
      :spouse_phone_number => "Spouse Phone Number"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/First Name/)
    expect(rendered).to match(/Last Name/)
    expect(rendered).to match(/Gender/)
    expect(rendered).to match(/Nric/)
    expect(rendered).to match(/Socso/)
    expect(rendered).to match(/Phone Number/)
    expect(rendered).to match(/Spouse Name/)
    expect(rendered).to match(/Spouse Phone Number/)
  end
end
