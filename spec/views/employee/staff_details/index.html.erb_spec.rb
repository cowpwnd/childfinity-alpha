require 'rails_helper'

RSpec.describe "employee/staff_details/index", type: :view do
  before(:each) do
    assign(:employee_staff_details, [
      Employee::StaffDetail.create!(
        :first_name => "First Name",
        :last_name => "Last Name",
        :gender => "Gender",
        :nric => "Nric",
        :socso_id => "Socso",
        :phone_number => "Phone Number",
        :spouse_name => "Spouse Name",
        :spouse_phone_number => "Spouse Phone Number"
      ),
      Employee::StaffDetail.create!(
        :first_name => "First Name",
        :last_name => "Last Name",
        :gender => "Gender",
        :nric => "Nric",
        :socso_id => "Socso",
        :phone_number => "Phone Number",
        :spouse_name => "Spouse Name",
        :spouse_phone_number => "Spouse Phone Number"
      )
    ])
  end

  it "renders a list of employee/staff_details" do
    render
    assert_select "tr>td", :text => "First Name".to_s, :count => 2
    assert_select "tr>td", :text => "Last Name".to_s, :count => 2
    assert_select "tr>td", :text => "Gender".to_s, :count => 2
    assert_select "tr>td", :text => "Nric".to_s, :count => 2
    assert_select "tr>td", :text => "Socso".to_s, :count => 2
    assert_select "tr>td", :text => "Phone Number".to_s, :count => 2
    assert_select "tr>td", :text => "Spouse Name".to_s, :count => 2
    assert_select "tr>td", :text => "Spouse Phone Number".to_s, :count => 2
  end
end
