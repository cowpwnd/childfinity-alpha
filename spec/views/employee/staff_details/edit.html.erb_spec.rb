require 'rails_helper'

RSpec.describe "employee/staff_details/edit", type: :view do
  before(:each) do
    @employee_staff_detail = assign(:employee_staff_detail, Employee::StaffDetail.create!(
      :first_name => "MyString",
      :last_name => "MyString",
      :gender => "MyString",
      :nric => "MyString",
      :socso_id => "MyString",
      :phone_number => "MyString",
      :spouse_name => "MyString",
      :spouse_phone_number => "MyString"
    ))
  end

  it "renders the edit employee_staff_detail form" do
    render

    assert_select "form[action=?][method=?]", employee_staff_detail_path(@employee_staff_detail), "post" do

      assert_select "input[name=?]", "employee_staff_detail[first_name]"

      assert_select "input[name=?]", "employee_staff_detail[last_name]"

      assert_select "input[name=?]", "employee_staff_detail[gender]"

      assert_select "input[name=?]", "employee_staff_detail[nric]"

      assert_select "input[name=?]", "employee_staff_detail[socso_id]"

      assert_select "input[name=?]", "employee_staff_detail[phone_number]"

      assert_select "input[name=?]", "employee_staff_detail[spouse_name]"

      assert_select "input[name=?]", "employee_staff_detail[spouse_phone_number]"
    end
  end
end
