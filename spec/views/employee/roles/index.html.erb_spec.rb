require 'rails_helper'

RSpec.describe "employee/roles/index", type: :view do
  before(:each) do
    assign(:employee_roles, [
      Employee::Role.create!(
        :name => "Name"
      ),
      Employee::Role.create!(
        :name => "Name"
      )
    ])
  end

  it "renders a list of employee/roles" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
  end
end
