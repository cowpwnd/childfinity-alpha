require 'rails_helper'

RSpec.describe "employee/roles/show", type: :view do
  before(:each) do
    @employee_role = assign(:employee_role, Employee::Role.create!(
      :name => "Name"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
  end
end
