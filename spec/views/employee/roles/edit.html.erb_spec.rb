require 'rails_helper'

RSpec.describe "employee/roles/edit", type: :view do
  before(:each) do
    @employee_role = assign(:employee_role, Employee::Role.create!(
      :name => "MyString"
    ))
  end

  it "renders the edit employee_role form" do
    render

    assert_select "form[action=?][method=?]", employee_role_path(@employee_role), "post" do

      assert_select "input[name=?]", "employee_role[name]"
    end
  end
end
