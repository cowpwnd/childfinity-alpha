require 'rails_helper'

RSpec.describe "company/branches/index", type: :view do
  before(:each) do
    @company = Company::Headquarter.new
    @company.name = 'Matrix'
    @company.registration_id = '12345'
    @company.save

    @company2 = Company::Headquarter.new
    @company2.name = 'Matrix'
    @company2.registration_id = '12345'
    @company2.save
    assign(:company_branches, [
      Company::Branch.create!(
        :name => "Name",
        :street1 => "Street1",
        :street2 => "Street2",
        :postal_code => "Postal Code",
        :city => "City",
        :state => "State",
        :country => "Country",
        :company_headquarter => @company
      ),
      Company::Branch.create!(
        :name => "Name",
        :street1 => "Street1",
        :street2 => "Street2",
        :postal_code => "Postal Code",
        :city => "City",
        :state => "State",
        :country => "Country",
        :company_headquarter => @company2
      )
    ])
  end

  it "renders a list of company/branches" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Street1".to_s, :count => 2
    assert_select "tr>td", :text => "Street2".to_s, :count => 2
    assert_select "tr>td", :text => "Postal Code".to_s, :count => 2
    assert_select "tr>td", :text => "City".to_s, :count => 2
    assert_select "tr>td", :text => "State".to_s, :count => 2
    assert_select "tr>td", :text => "Country".to_s, :count => 2
    assert_select "tr>td", :text => "Matrix".to_s, :count => 2
  end
end
