require 'rails_helper'

RSpec.describe "company/branches/edit", type: :view do
  before(:each) do
    @company = Company::Headquarter.new
    @company.name = 'Matrix'
    @company.registration_id = '12345'
    @company.save

    @company_branch = assign(:company_branch, Company::Branch.create!(
      :name => "MyString",
      :street1 => "MyString",
      :street2 => "MyString",
      :postal_code => "MyString",
      :city => "MyString",
      :state => "MyString",
      :country => "MyString",
      :company_headquarter => @company
    ))
  end

  it "renders the edit company_branch form" do
    render

    assert_select "form[action=?][method=?]", company_branch_path(@company_branch), "post" do

      assert_select "input[name=?]", "company_branch[name]"

      assert_select "input[name=?]", "company_branch[street1]"

      assert_select "input[name=?]", "company_branch[street2]"

      assert_select "input[name=?]", "company_branch[postal_code]"

      assert_select "input[name=?]", "company_branch[city]"

      assert_select "input[name=?]", "company_branch[state]"

      assert_select "input[name=?]", "company_branch[country]"

      assert_select "input[name=?]", "company_branch[company_headquarter_id]"
    end
  end
end
