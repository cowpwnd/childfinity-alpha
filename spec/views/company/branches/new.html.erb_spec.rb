require 'rails_helper'

RSpec.describe "company/branches/new", type: :view do
  before(:each) do
    assign(:company_branch, Company::Branch.new(
      :name => "MyString",
      :street1 => "MyString",
      :street2 => "MyString",
      :postal_code => "MyString",
      :city => "MyString",
      :state => "MyString",
      :country => "MyString",
      :company_headquarter => nil
    ))
  end

  it "renders new company_branch form" do
    render

    assert_select "form[action=?][method=?]", company_branches_path, "post" do

      assert_select "input[name=?]", "company_branch[name]"

      assert_select "input[name=?]", "company_branch[street1]"

      assert_select "input[name=?]", "company_branch[street2]"

      assert_select "input[name=?]", "company_branch[postal_code]"

      assert_select "input[name=?]", "company_branch[city]"

      assert_select "input[name=?]", "company_branch[state]"

      assert_select "input[name=?]", "company_branch[country]"

      assert_select "input[name=?]", "company_branch[company_headquarter_id]"
    end
  end
end
