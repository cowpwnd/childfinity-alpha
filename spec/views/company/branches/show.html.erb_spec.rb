require 'rails_helper'

RSpec.describe "company/branches/show", type: :view do
  before(:each) do
    @company = Company::Headquarter.new
    @company.name = 'Matrix'
    @company.registration_id = '12345'
    @company.save

    @company_branch = assign(:company_branch, Company::Branch.create!(
      :name => "Name",
      :street1 => "Street1",
      :street2 => "Street2",
      :postal_code => "Postal Code",
      :city => "City",
      :state => "State",
      :country => "Country",
      :company_headquarter => @company
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/Street1/)
    expect(rendered).to match(/Street2/)
    expect(rendered).to match(/Postal Code/)
    expect(rendered).to match(/City/)
    expect(rendered).to match(/State/)
    expect(rendered).to match(/Country/)
    expect(rendered).to match(//)
  end
end
