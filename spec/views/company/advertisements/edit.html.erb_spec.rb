require 'rails_helper'
RSpec.describe "company/advertisements/edit", type: :view do
  before(:each) do

    @company = Company::Headquarter.new
    @company.name = "Sample Company"
    @company.save

    @branch = Company::Branch.new
    @branch.name = "Sample Company/Branch 1"
    @branch.company_headquarter = @company

    @branch.save

    @company_advertisement = assign(:company_advertisement, Company::Advertisement.create!(
        :name => "MyString",
        :description => "MyString",
        :ads_image => "",
        :ads_duration => 1,
        :ads_active => false,
        :company_branch => @branch
    ))
  end

  it "renders the edit company_advertisement form" do
    render

    assert_select "form[action=?][method=?]", company_advertisement_path(@company_advertisement), "post" do

      assert_select "input[name=?]", "company_advertisement[name]"

      assert_select "input[name=?]", "company_advertisement[description]"

      assert_select "input[name=?]", "company_advertisement[ads_image]"

      assert_select "input[name=?]", "company_advertisement[ads_duration]"

      assert_select "input[name=?]", "company_advertisement[ads_active]"

      assert_select "input[name=?]", "company_advertisement[company_branch_id]"
    end
  end
end
