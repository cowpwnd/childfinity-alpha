require 'rails_helper'

RSpec.describe "company/advertisements/show", type: :view do
  before(:each) do
    @company = Company::Headquarter.new
    @company.name = "Sample Company"
    @company.save

    @branch = Company::Branch.new
    @branch.name = "Sample Company/Branch 1"
    @branch.company_headquarter = @company

    @branch.save

    @company_advertisement = assign(:company_advertisement, Company::Advertisement.create!(
      :name => "Name",
      :description => "Description",
      :ads_image => "",
      :ads_duration => 2,
      :ads_active => false,
      :company_branch => @branch
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/Description/)
    expect(rendered).to match(//)
    expect(rendered).to match(/2/)
    expect(rendered).to match(/false/)
    expect(rendered).to match(//)
  end
end
