require 'rails_helper'

RSpec.describe "company/advertisements/index", type: :view do
  before(:each) do
    @company = Company::Headquarter.new
    @company.name = "Sample Company"
    @company.save

    @branch = Company::Branch.new
    @branch.name = "Sample Company/Branch 1"
    @branch.company_headquarter = @company

    @branch.save

    @branch2 = Company::Branch.new
    @branch2.name = "Sample Company/Branch 2"
    @branch2.company_headquarter = @company

    @branch2.save

    assign(:company_advertisements, [
      Company::Advertisement.create!(
        :name => "Name",
        :description => "Description",
        :ads_image => "",
        :ads_duration => 2,
        :ads_active => false,
        :company_branch => @branch
      ),
      Company::Advertisement.create!(
        :name => "Name",
        :description => "Description",
        :ads_image => "",
        :ads_duration => 2,
        :ads_active => false,
        :company_branch => @branch2
      )
    ])
  end

  it "renders a list of company/advertisements" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Description".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
