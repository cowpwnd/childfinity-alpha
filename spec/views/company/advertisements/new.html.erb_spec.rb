require 'rails_helper'

RSpec.describe "company/advertisements/new", type: :view do
  before(:each) do
    assign(:company_advertisement, Company::Advertisement.new(
      :name => "MyString",
      :description => "MyString",
      :ads_image => "",
      :ads_duration => 1,
      :ads_active => false,
      :company_branch => nil
    ))
  end

  it "renders new company_advertisement form" do
    render

    assert_select "form[action=?][method=?]", company_advertisements_path, "post" do

      assert_select "input[name=?]", "company_advertisement[name]"

      assert_select "input[name=?]", "company_advertisement[description]"

      assert_select "input[name=?]", "company_advertisement[ads_image]"

      assert_select "input[name=?]", "company_advertisement[ads_duration]"

      assert_select "input[name=?]", "company_advertisement[ads_active]"

      assert_select "input[name=?]", "company_advertisement[company_branch_id]"
    end
  end
end
