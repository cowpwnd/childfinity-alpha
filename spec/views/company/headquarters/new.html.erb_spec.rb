require 'rails_helper'

RSpec.describe "company/headquarters/new", type: :view do
  before(:each) do
    assign(:company_headquarter, Company::Headquarter.new(
      :name => "MyString",
      :registration_id => "MyString"
    ))
  end

  it "renders new company_headquarter form" do
    render

    assert_select "form[action=?][method=?]", company_headquarters_path, "post" do

      assert_select "input[name=?]", "company_headquarter[name]"

      assert_select "input[name=?]", "company_headquarter[registration_id]"
    end
  end
end
