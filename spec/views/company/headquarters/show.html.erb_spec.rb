require 'rails_helper'

RSpec.describe "company/headquarters/show", type: :view do
  before(:each) do
    @company_headquarter = assign(:company_headquarter, Company::Headquarter.create!(
      :name => "Name",
      :registration_id => "Registration"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/Registration/)
  end
end
