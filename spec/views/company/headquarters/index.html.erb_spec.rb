require 'rails_helper'

RSpec.describe "company/headquarters/index", type: :view do
  before(:each) do
    assign(:company_headquarters, [
      Company::Headquarter.create!(
        :name => "Name",
        :registration_id => "Registration"
      ),
      Company::Headquarter.create!(
        :name => "Name",
        :registration_id => "Registration"
      )
    ])
  end

  it "renders a list of company/headquarters" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Registration".to_s, :count => 2
  end
end
