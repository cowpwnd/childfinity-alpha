require 'rails_helper'

RSpec.describe "company/headquarters/edit", type: :view do
  before(:each) do
    @company_headquarter = assign(:company_headquarter, Company::Headquarter.create!(
      :name => "MyString",
      :registration_id => "MyString"
    ))
  end

  it "renders the edit company_headquarter form" do
    render

    assert_select "form[action=?][method=?]", company_headquarter_path(@company_headquarter), "post" do

      assert_select "input[name=?]", "company_headquarter[name]"

      assert_select "input[name=?]", "company_headquarter[registration_id]"
    end
  end
end
